import unittest
import hello


class TestHello():
    def test_add(self):
        instance = hello.Hello()
        result = instance.add(2, 3)
        self.assertEqual(result, 5)
        result = instance.add(3, 5)
        self.assertEqual(result, 7)


if __name__ == "__main__":
    unittest.main()
